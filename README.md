# Tkinter app for merging tables

App for merging tables using python's built-in GUI module tkinter. The project is a simple structered stand-alone python script containing all functionalities (GUI, merging feature) of the app and has to be executed from the command line. The purpose of the project is to provide a convenient GUI for merging tables on the one hand, and to get more familiar with Tkinter on the other hand. Internally, the app uses pandas for merging.

## Requirements

Requires a python version >= 3.7.

pandas==1.4.2

## Usage

To use the app simply do the following steps in your console:

1. Clone repository into a designated working directory.
    ```
    git clone https://gitlab.com/rizimmer/dataframe-merger.git
    ```
2. Navigate to the working directory and run the script:
    ```
    cd path/to/working/directory
    python dataframe_merger.py
    ```


## Demo
Example usage:

![](./images/example-usage.png "Title Text")

## References
- Tables from w3schools SQL-tutorial: https://www.w3schools.com/sql/

