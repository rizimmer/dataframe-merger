
import os
import csv
import pandas as pd
import tkinter as tk

from tkinter import ttk
from tkinter import filedialog
from tkinter import font
from tkinter import messagebox
from itertools import islice
from functools import reduce

from ctypes import windll
windll.shcore.SetProcessDpiAwareness(1)

max_row_read = 200
width_nbs = 1200
width_main_window = 1400
height_nbs = 300
height_main_window = 900

# configuration
initialdir_saved_merges = './data'
initialdir_dataframes = './data'
config_max_row_show = 200
config_sep = ";"
config_dec = "."



class ScrollbarTreeview(tk.Frame):
    def __init__(self,master, columns, width, height, name):
        super().__init__(master, width=width, height=height)
        self.propagate(False)
        tree = ttk.Treeview(self, height=10, columns=columns, selectmode="extended")
        x_sb = tk.Scrollbar(self,orient='horizontal')
        y_sb = tk.Scrollbar(self,orient='vertical')
        x_sb.config(command=tree.xview)
        y_sb.config(command=tree.yview)
        tree.config(xscrollcommand=x_sb.set,yscrollcommand=y_sb.set)
        tree.grid(column=0,row=0,sticky='nsew')
        x_sb.grid(column=0,row=1,sticky='ew')
        y_sb.grid(column=1,row=0,sticky='ns')
        self.grid_propagate(0)
        self.columnconfigure(0,weight=1)
        self.rowconfigure(0,weight=1)
        self.name = name
        self.tree = tree
        self.x_sb = x_sb
        self.y_sb = y_sb

class NotebookWithDataframeTabs(ttk.Notebook):
    def __init__(self, master, width, height, filenames, tabnames):
        super().__init__(master, width=width, height=height)
        self.filenames=filenames
        self.tabnames=tabnames
        self.dict_of_attribute_lists={}
        self.list_of_key_attributes=[]
        self.savedmerges={}

class CheckBoxesAttributeSelection():
    def __init__(self, frame, attributes):
        self.frame = frame
        self.attributes = attributes
        self.check_buttons = {}
        self.check_buttons_vars = {}
        self.sel_values = []

    def change_sel_values(self):
        l = []
        for attr in self.attributes:
            if self.check_buttons_vars[attr].get() == 1:
                l.append(attr)

    def create(self):
        canvas = tk.Canvas(self.frame, height=60, width=width_nbs)
        canvas_x_sb = tk.Scrollbar(self.frame, orient='horizontal')

        canvas_x_sb.config(command=canvas.xview)
        canvas.config(xscrollcommand=canvas_x_sb.set)

        canvas.grid(row=0, column=0, sticky='nesw')
        canvas_x_sb.grid(row=1, column=0, sticky='ew')

        frame_checkboxes = tk.Frame(canvas)
        frame_checkboxes.grid(row=0, column=0)

        for i, attr in enumerate(self.attributes):
            checktext = tk.Label(frame_checkboxes, text=attr)

            self.check_buttons_vars[attr] = tk.IntVar(value=1)
            self.check_buttons[attr] = tk.Checkbutton(frame_checkboxes, text="" , variable = self.check_buttons_vars[attr],
                             onvalue = 1, offvalue = 0, command=self.change_sel_values)

            checktext.grid(row=0, column=i, padx=(10,10))
            self.check_buttons[attr].grid(row=1, column=i, padx=(10,10))

        canvas.create_window(0, 60, anchor='sw', window=frame_checkboxes)
        frame_checkboxes.update_idletasks()
        canvas.config(scrollregion=canvas.bbox("all"))



def add_table_as_notebook_tab(notebook, filename):
    """
    Add the dataframe specificied in the path filename as treeview to a new tab in notebook.

    param notebook: The notebook to which we append the dataframe
    param filename: The path to the dataframe (saved as .csv-file)
    """

    notebook.filenames.append(filename)

    tab_notebook = csv_to_notebook_tab(notebook, filename)
    notebook.tabnames.append(tab_notebook)

    title=os.path.basename(filename)
    notebook.add(tab_notebook,text=title)
    notebook.select(notebook.tabs()[notebook.index('end')-1])

def merge_dataframes(notebook, notebook_top, df_names, join_how):
    """
    Merge the dataframes specified by df_names in a new tab in notebook.

    param notebook: The notebook to which we append the dataframe merge
    param df_names: Names of dataframes in their base form ('vtgkz', 'verskz', etc.).
    """

    if len(df_names)>1:

        tables = {}
        id_cols = notebook_top.list_of_key_attributes

        for name in df_names:
            df_path = os.path.join(initialdir_dataframes, name + '.csv')
            tables[name] = pd.read_csv(df_path, sep=config_sep, decimal=config_dec)

        print("id_cols:", id_cols)
        print("tables.values():", tables.values())

        # perform merge
        merged_df = reduce(lambda left, right:
                          pd.merge(left, right, how = join_how, on = id_cols), list(tables.values()))

        file_name = os.path.join(initialdir_saved_merges, '_'.join(df_names) + "_" + join_how + "_join" + ".csv")
        merged_df.to_csv(file_name, sep=';', index=False)

        tab_notebook = csv_to_notebook_tab(notebook, file_name_csv=file_name)
        if(os.path.exists(file_name) and os.path.isfile(file_name)) and file_name.endswith('.csv'):
            os.remove(file_name)

        notebook.filenames.append(file_name)
        notebook.savedmerges[file_name] = merged_df
        notebook.tabnames.append(tab_notebook)

        title=os.path.basename(file_name)
        notebook.add(tab_notebook,text=title)
        notebook.select(notebook.tabs()[notebook.index('end')-1])

    else:
        messagebox.showinfo("Info", "Load tables first!")


def csv_to_notebook_tab(notebook, file_name_csv):
    """
    Take the dataframe (.csv-file) specified in path file_name_csv and write it as tab to the notebook.

    param notebook: The notebook to which we append the dataframe
    param file_name_csv: The path to the dataframe (.csv-file)
    """

    with open(file_name_csv) as f:
        reader = csv.DictReader(f, delimiter=';')

        columns = tuple(reader.fieldnames)
        tab_notebook = ScrollbarTreeview(notebook, columns, width=width_nbs, height=height_nbs, name=file_name_csv)
        tree = tab_notebook.tree

        tree.heading('#0', text="#", anchor=tk.E)
        tree.column('#0', stretch=tk.NO, anchor=tk.E, minwidth=20, width=font_default.measure("000000  "))
        for col in columns:
            width_col = font_default.measure(col+"         ")
            tree.heading(col, text=col, anchor=tk.CENTER)
            tree.column(col, stretch=False, anchor=tk.CENTER, minwidth=20, width=width_col)

        for ind, row in enumerate(islice(reader,0,max_row_read)):
            if ind < max_row_read:
                row_values = tuple(row.values())
                tree.insert("", tk.END, text=str(ind), iid=ind, values=row_values, tag='gray')
            if ind == max_row_read-1:
                tree.insert("", tk.END, text="...", values=tuple(list("..." for _ in range(len(columns)))))


        update_table_attributes(notebook, file_name_csv, columns)

        return tab_notebook

def update_table_attributes(notebook, table_name, columns=None, remove_table=False):
    if len(notebook.filenames)>0:
        if remove_table == True:
            del notebook.dict_of_attribute_lists[table_name]
        else:
            notebook.dict_of_attribute_lists[table_name] = list(columns)

        # list of the attribute lists that are saved in each dictionary value
        l = list(notebook.dict_of_attribute_lists.values())

        # Set key attributes as intersection of all attribute lists (https://stackoverflow.com/questions/3852780/python-intersection-of-multiple-lists)
        notebook.list_of_key_attributes = list(reduce(set.intersection, [set(item) for item in l]))
    else:
        notebook.list_of_key_attributes = []

def add_checkboxes_selection(container_checkboxes, list_attributes):
    CheckBoxesAttributeSelection(container_checkboxes, attributes=list_attributes).create()

# Functions called by GUI-interaction
def button_open_dataframe(notebook, container_checkboxes):
    filename = filedialog.askopenfilename(initialdir=initialdir_dataframes)

    if os.path.isfile(filename):
        add_table_as_notebook_tab(notebook, filename)
        add_checkboxes_selection(container_checkboxes, notebook.list_of_key_attributes)

def button_merge_dataframes(notebook_bottom, notebook_top, join_how):
    filenames_for_merge = notebook_top.filenames
    df_names = [os.path.basename(name).split(".")[0] for name in filenames_for_merge]
    print("df_names:", df_names)
    merge_dataframes(notebook_bottom, notebook_top, df_names, join_how=join_how)

def button_open_saved_merge(notebook):
    filename = filedialog.askopenfilename(initialdir=initialdir_saved_merges)
    add_table_as_notebook_tab(notebook, filename)

def button_delete_tab(notebook, container_checkboxes):
    for item in notebook.winfo_children():
        if str(item) == (notebook.select()):
            notebook.tabnames.remove(item)
            notebook.filenames.remove(item.name)
            item.destroy()
            
            update_table_attributes(notebook, item.name, columns=None, remove_table=True)
            add_checkboxes_selection(container_checkboxes, notebook.list_of_key_attributes)
            return

def button_delete_tab_merges(notebook):
    for item in notebook.winfo_children():
        if str(item) == (notebook.select()):
            notebook.tabnames.remove(item)
            notebook.filenames.remove(item.name)
            item.destroy()
            return

def button_save_merge(notebook):
    for item in notebook.winfo_children():
        if str(item) == (notebook.select()):
            initial_file_name = os.path.basename(item.name).split(".")[0]
            filename = filedialog.asksaveasfilename(initialdir=initialdir_saved_merges,
                                                    initialfile=initial_file_name,
                                                    defaultextension=".csv",
                                                    filetypes=(("CSV", "*.csv"),
                                                               ("All files", "*.*")
                                                    )
            )
            
            # Save selected table
            notebook.savedmerges[item.name].to_csv(filename, sep=';', index=False)

            return


        # file_name = os.path.join(initialdir_saved_merges, '_'.join(df_names) + "_" + join_how + "_join" + ".csv")
        # merged_df.to_csv(file_name, sep=';', index=False)

        # tab_notebook = csv_to_notebook_tab(notebook, file_name_csv=file_name)
        # if(os.path.exists(file_name) and os.path.isfile(file_name)) and file_name.endswith('.csv'):
        #     os.remove(file_name)

        # notebook.filenames.append(file_name)
        # notebook.savedmerges[file_name] = merged_df
        # notebook.tabnames.append(tab_notebook)




def main():
    global font_default

    root = tk.Tk()
    root.title('Dataframe viewer')
    root.geometry(str(width_main_window)+"x"+str(height_main_window))
    font_default = font.nametofont("TkDefaultFont")
    font_height = tk.font.Font(font='TkDefaultFont').metrics('linespace')

    style = ttk.Style()
    style.configure("Treeview", rowheight=int(font_height*1.2))

    # Initialise widgets
    tabs_top, tabs_bottom = [], []
    filenames_bottom_nb, filenames_top_nb = [], []

    notebook_top = NotebookWithDataframeTabs(root, width_nbs, height_nbs, filenames=filenames_top_nb, tabnames=tabs_top)
    notebook_bottom = NotebookWithDataframeTabs(root, width_nbs, height_nbs, filenames=filenames_bottom_nb, tabnames=tabs_bottom)

    label_checkboxes = tk.Label(root, text = "Choose key attributes for merging:")

    container_checkboxes = tk.Frame(root)
    CheckBoxesAttributeSelection(container_checkboxes, attributes=[]).create()

    # Buttons
    b_top_1 = tk.Button(root, text='Add table', width=20,
                   command=lambda: button_open_dataframe(notebook_top, container_checkboxes))
    b_top_2 = tk.Button(root, text='Delete selected table', width=20,
                   command=lambda: button_delete_tab(notebook_top, container_checkboxes))

    b_bot_1_1 = tk.Button(root, text='Inner join', width=20,
                   command=lambda: button_merge_dataframes(notebook_bottom, notebook_top=notebook_top, join_how="inner"))
    b_bot_1_2 = tk.Button(root, text='Outer join', width=20,
                   command=lambda: button_merge_dataframes(notebook_bottom, notebook_top=notebook_top, join_how="outer"))
    b_bot_1_3 = tk.Button(root, text='Left join', width=20,
                   command=lambda: button_merge_dataframes(notebook_bottom, notebook_top=notebook_top, join_how="left"))
    b_bot_1_4 = tk.Button(root, text='Right join', width=20,
                   command=lambda: button_merge_dataframes(notebook_bottom, notebook_top=notebook_top, join_how="right"))

    b_bot_2_1 = tk.Button(root, text='Save merge', width=20,
                command=lambda: button_save_merge(notebook_bottom))
    b_bot_2_2 = tk.Button(root, text='Load saved merge', width=20,
                   command=lambda: button_open_saved_merge(notebook_bottom))
    b_bot_2_3 = tk.Button(root, text='Delete selected merge', width=20,
                   command=lambda: button_delete_tab_merges(notebook_bottom))

    # Position widgets
    b_pad_x_left, b_pad_x_right = 20, 20
    notebook_top.grid(row=1, column=0, sticky='w', padx=(20,20))
    label_checkboxes.grid(row=2, column=0, sticky='sw', padx=(20,20), pady=(60,5))
    container_checkboxes.grid(row=3, column=0, sticky='nesw', padx=(20,20))
    notebook_bottom.grid(row=6, column=0, sticky='w', padx=(20,20))

    b_top_1.grid(row=0,column=0, sticky='w', pady=(20,5), padx=(b_pad_x_left, b_pad_x_right))
    b_bot_1_1.grid(row=4,column=0, sticky='w', pady=(40,5), padx=(b_pad_x_left, b_pad_x_right))
    b_bot_2_1.grid(row=5,column=0, sticky='w', pady=(0,5), padx=(b_pad_x_left, b_pad_x_right))

    root.update()
    b_top_2.grid(row=0,column=0, sticky='w', pady=(20,5), padx=(b_top_1.winfo_width() + b_pad_x_left, b_pad_x_right))

    b_bot_1_2.grid(row=4,column=0, sticky='w', pady=(40,5), padx=(b_bot_1_1.winfo_width() + b_pad_x_left, b_pad_x_right))
    b_bot_1_3.grid(row=4,column=0, sticky='w', pady=(40,5), padx=(2*b_bot_1_1.winfo_width() + b_pad_x_left, b_pad_x_right))
    b_bot_1_4.grid(row=4,column=0, sticky='w', pady=(40,5), padx=(3*b_bot_1_1.winfo_width() + b_pad_x_left, b_pad_x_right))

    b_bot_2_2.grid(row=5,column=0, sticky='w', pady=(0,5), padx=(b_bot_1_1.winfo_width() + b_pad_x_left, b_pad_x_right))
    b_bot_2_3.grid(row=5,column=0, sticky='w', pady=(0,5), padx=(2*b_bot_1_1.winfo_width() + b_pad_x_left, b_pad_x_right))


    # Add menu
    menubar = tk.Menu(root)
    filemenu = tk.Menu(menubar, tearoff=0)
    filemenu.add_command(label='Exit', command=exit)
    menubar.add_cascade(label='File', menu=filemenu)

    root.config(menu=menubar)
    root.mainloop()

if __name__ == '__main__':
    main()



